import copy
import azure.functions as func
import logging
import joblib
import pandas as pd
from surprise import Dataset, Reader

# Création de l'application Azure Functions
app = func.FunctionApp(http_auth_level=func.AuthLevel.ANONYMOUS)

# Cache pour stocker le modèle et les données
cache = {}

def load_model_and_data():
    """Charge le modèle et les données nécessaires et les stocke dans le cache."""
    try:
        cache['loaded_model'] = joblib.load('svd.joblib')
        user_article_interaction_scaled = pd.read_csv('user_article_interaction_scaled.csv')
        cache['all_categories'] = user_article_interaction_scaled['article_id'].unique()
    except Exception as e:
        logging.error(f"Erreur lors du chargement du modèle ou des données: {e}")
        raise

def get_recommendations(user_id, article_ids):
    """Génère des recommandations d'articles pour un utilisateur donné.
    
    Args:
        user_id (int): L'ID de l'utilisateur.
        article_ids (list): Liste des IDs d'articles déjà vus par l'utilisateur.
    
    Returns:
        list: Liste des IDs des articles recommandés.
    """
    loaded_model = copy.copy(cache['loaded_model'])
    all_categories = cache['all_categories']

    # Convertir les article_ids en liste d'entiers
    article_ids = list(map(int, article_ids))

    # Préparer les données pour la prédiction
    new_user_data = pd.DataFrame({
        'user_id': [int(user_id)]*len(article_ids), 
        'article_id': article_ids, 
        'rating': [1]*len(article_ids)
    })
    
    new_user_all_categories = pd.DataFrame({'article_id': all_categories})
    new_user_all_categories['user_id'] = int(user_id)
    rating_map = new_user_data.set_index('article_id')['rating'].to_dict()
    new_user_all_categories['rating'] = new_user_all_categories['article_id'].map(rating_map).fillna(0)

    reader = Reader(rating_scale=(0, 5))
    data = Dataset.load_from_df(new_user_all_categories[['user_id', 'article_id', 'rating']], reader)
    trainset = data.build_full_trainset()

    loaded_model.fit(trainset)

    # Faire des prédictions pour l'utilisateur donné
    predictions = []
    for article_id in all_categories:
        if article_id not in article_ids:  # Exclure les articles déjà lus
            predictions.append((article_id, loaded_model.predict(int(user_id), article_id).est))

    # Trier les prédictions par score décroissant et prendre les 5 premières
    recommendations = sorted(predictions, key=lambda x: x[1], reverse=True)[:5]

    # Renvoyer les IDs des articles recommandés
    recommended_article_ids = [str(rec[0]) for rec in recommendations]
    return recommended_article_ids

@app.route(route="http_trigger")
def http_trigger(req: func.HttpRequest) -> func.HttpResponse:
    """Fonction déclenchée par une requête HTTP."""
    logging.info('La fonction HTTP trigger a traité une requête.')

    name = req.params.get('name')
    if not name:
        try:
            req_body = req.get_json()
        except ValueError:
            pass
        else:
            name = req_body.get('name')

    if name:
        return func.HttpResponse(f"Bonjour, {name}. Cette fonction HTTP a été exécutée avec succès.")
    else:
        return func.HttpResponse(
            "Cette fonction HTTP a été exécutée avec succès. Passez un nom dans la chaîne de requête ou dans le corps de la requête pour une réponse personnalisée.",
            status_code=200
        )

@app.route(route="recommend", methods=['GET'])
def recommend(req: func.HttpRequest) -> func.HttpResponse:
    """Fonction de recommandation déclenchée par une requête HTTP."""
    logging.info('La fonction de recommandation a reçu une requête.')

    try:
        user_id = req.params.get('user_id')
        article_ids = req.params.get('article_ids')

        if not user_id or not article_ids:
            return func.HttpResponse(
                "Veuillez fournir user_id et article_ids dans la chaîne de requête.",
                status_code=400
            )

        # Charger le modèle et les données nécessaires
        if 'loaded_model' not in cache or 'all_categories' not in cache:
            load_model_and_data()

        # Obtenir les recommandations
        recommended_article_ids = get_recommendations(user_id, article_ids.split(','))

        return func.HttpResponse(",".join(recommended_article_ids))
    except Exception as e:
        logging.error(f"Erreur lors du traitement: {e}")
        return func.HttpResponse(
            "Une erreur s'est produite lors du traitement de votre demande. Veuillez réessayer plus tard.",
            status_code=500
        )
