import pandas as pd
import glob
import os
from surprise import SVD
from surprise import Dataset, Reader
import joblib
from sklearn.preprocessing import MinMaxScaler

class DataPreparation:
    """
    Classe pour préparer les données nécessaires pour l'entraînement du modèle SVD.
    """

    def __init__(self, articles_path, clicks_sample_path, clicks_hour_pattern, clicks_hour_combined_path):
        self.articles_path = articles_path
        self.clicks_sample_path = clicks_sample_path
        self.clicks_hour_pattern = clicks_hour_pattern
        self.clicks_hour_combined_path = clicks_hour_combined_path

    def load_articles_metadata(self):
        """
        Charger les métadonnées des articles.
        
        Returns:
            pd.DataFrame: Le DataFrame des métadonnées des articles.
        """
        return pd.read_csv(self.articles_path)

    def load_clicks_hour(self):
        """
        Charger les données de clics horaires.
        
        Returns:
            pd.DataFrame: Le DataFrame des clics horaires.
        """
        clicks_hour_files = glob.glob(self.clicks_hour_pattern)
        clicks_hour = pd.DataFrame()

        for filename in clicks_hour_files:
            temp_df = pd.read_csv(filename)
            clicks_hour = pd.concat([clicks_hour, temp_df], ignore_index=True)

        return clicks_hour

    def filter_invalid_sessions(self, clicks_hour):
        """
        Supprimer les sessions avec un nombre incorrect de session_size.
        
        Args:
            clicks_hour (pd.DataFrame): Le DataFrame des clics horaires.
        
        Returns:
            pd.DataFrame: Le DataFrame des clics horaires filtrés.
        """
        incorrect_session_ids = clicks_hour.groupby('session_id')['session_size'].count().reset_index(name='count')
        incorrect_session_ids = incorrect_session_ids[incorrect_session_ids['count']!= clicks_hour['session_size'].unique()[0]]['session_id']
        clicks_hour = clicks_hour[~clicks_hour['session_id'].isin(incorrect_session_ids)]
        return clicks_hour

    def drop_duplicates_per_session(self, clicks_hour):
        """
        Supprimer les répétitions d'article au sein d'une même session.
        
        Args:
            clicks_hour (pd.DataFrame): Le DataFrame des clics horaires.
        
        Returns:
            pd.DataFrame: Le DataFrame des clics horaires filtrés.
        """
        clicks_hour = clicks_hour.drop_duplicates(subset=['session_id', 'click_article_id'])
        return clicks_hour

    def filter_clicks_before_session_start(self, clicks_hour):
        """
        Supprimer les clics dont la date est antérieure à session_start.
                
        Args:
            clicks_hour (pd.DataFrame): Le DataFrame des clics horaires.
        
        Returns:
            pd.DataFrame: Le DataFrame des clics horaires filtrés.
        """
        clicks_hour = clicks_hour[clicks_hour['click_timestamp'] >= clicks_hour['session_start']]
        return clicks_hour

    def filter_clicks_with_invalid_article_id(self, clicks_hour, articles_metadata):
        """
        Supprimer les clics qui ont un click_article_id avec un id qui correspond à aucun article_id.
        
        Args:
            clicks_hour (pd.DataFrame): Le DataFrame des clics horaires.
            articles_metadata (pd.DataFrame): Le DataFrame des métadonnées des articles.
        
        Returns:
            pd.DataFrame: Le DataFrame des clics horaires filtrés.
        """
        clicks_hour = clicks_hour[clicks_hour['click_article_id'].isin(articles_metadata['article_id'])]
        return clicks_hour

    def merge_clicks_articles(self, clicks_hour, articles_metadata):
        """
        Fusionner les données de clics avec les métadonnées des articles.
        
        Args:
            clicks_hour (pd.DataFrame): Le DataFrame des clics horaires filtrés.
            articles_metadata (pd.DataFrame): Le DataFrame des métadonnées des articles.
        
        Returns:
            pd.DataFrame: Le DataFrame fusionné des clics et des articles.
        """
        return pd.merge(clicks_hour, articles_metadata, left_on='click_article_id', right_on='article_id', how='left')


class SVDModel:
    """
    Classe pour gérer l'entraînement et la sauvegarde du modèle SVD.
    """

    def __init__(self, model_path, interaction_path):
        self.model_path = model_path
        self.interaction_path = interaction_path

    def prepare_interactions(self, clicks):
        """
        Préparer les interactions utilisateur-article.
        
        Args:
            clicks (pd.DataFrame): Le DataFrame fusionné des clics et des articles.
        
        Returns:
            pd.DataFrame: Le DataFrame des interactions utilisateur-article avec les notes mises à l'échelle.
        """
        user_article_interaction = clicks.groupby(['user_id', 'article_id']).size().reset_index(name='rating')
        scaler = MinMaxScaler(feature_range=(1, 5))
        user_article_interaction['rating'] = scaler.fit_transform(user_article_interaction[['rating']])
        user_article_interaction.to_csv(self.interaction_path, index=False)
        return user_article_interaction

    def train_svd_model(self, interactions):
        """
        Entraîner le modèle SVD.
        
        Args:
            interactions (pd.DataFrame): Le DataFrame des interactions utilisateur-article.
        
        Returns:
            SVD: Le modèle SVD entraîné.
        """
        reader = Reader(rating_scale=(1, 5))
        data = Dataset.load_from_df(interactions[['user_id', 'article_id', 'rating']], reader)
        trainset = data.build_full_trainset()

        algo = SVD()
        algo.fit(trainset)
        return algo

    def save_model(self, model):
        """
        Sauvegarder le modèle SVD sur le disque.
        
        Args:
            model (SVD): Le modèle SVD entraîné.
        """
        joblib.dump(model, self.model_path)


def main():
    # Initialisation des chemins de fichiers
    print('Initialisation des chemins de fichiers')
    articles_path = 'data/RAW/articles_metadata.csv'
    clicks_sample_path = 'data/RAW/clicks_sample.csv'
    clicks_hour_pattern = "data/RAW/clicks/clicks_hour_*.csv"
    clicks_hour_combined_path = 'data/001/clicks_hour.csv'
    model_path = 'api/svd.joblib'
    interaction_path = 'api/user_article_interaction_scaled.csv'

    # Préparation des données
    print('Préparation des données')
    data_preparation = DataPreparation(articles_path, clicks_sample_path, clicks_hour_pattern, clicks_hour_combined_path)
    articles_metadata = data_preparation.load_articles_metadata()
    clicks_hour = data_preparation.load_clicks_hour()
    clicks_hour = data_preparation.filter_invalid_sessions(clicks_hour)
    clicks_hour = data_preparation.drop_duplicates_per_session(clicks_hour)
    clicks_hour = data_preparation.filter_clicks_before_session_start(clicks_hour)
    clicks_hour = data_preparation.filter_clicks_with_invalid_article_id(clicks_hour, articles_metadata)
    clicks = data_preparation.merge_clicks_articles(clicks_hour, articles_metadata)

    # Entraînement du modèle SVD
    print('Entraînement du modèle SVD')
    svd_model = SVDModel(model_path, interaction_path)
    interactions = svd_model.prepare_interactions(clicks)
    model = svd_model.train_svd_model(interactions)

    # Sauvegarde du modèle
    print('Sauvegarde du modèle')
    svd_model.save_model(model)

    print('Fini')


if __name__ == "__main__":
    main()