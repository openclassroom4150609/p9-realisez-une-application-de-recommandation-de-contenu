import copy
import azure.functions as func
import logging
import joblib
import pandas as pd
from surprise import Dataset, Reader

app = func.FunctionApp(http_auth_level=func.AuthLevel.ANONYMOUS)

@app.route(route="http_trigger")
def http_trigger(req: func.HttpRequest) -> func.HttpResponse:
    logging.info('Python HTTP trigger function processed a request.')

    name = req.params.get('name')
    if not name:
        try:
            req_body = req.get_json()
        except ValueError:
            pass
        else:
            name = req_body.get('name')

    if name:
        return func.HttpResponse(f"Hello, {name}. This HTTP triggered function executed successfully.")
    else:
        return func.HttpResponse(
             "This HTTP triggered function executed successfully. Pass a name in the query string or in the request body for a personalized response.",
             status_code=200
        )
    

cache = {}

@app.route(route="recommend", methods=['GET'])
def recommend(req: func.HttpRequest) -> func.HttpResponse:
    logging.info('Recommendation function received a request.')

    # Utiliser les données chargées dans le cache
    loaded_model = copy.copy(cache['loaded_model'])
    all_categories = cache['all_categories']

    try:
        user_id = req.params.get('user_id')
        article_ids = req.params.get('article_ids')

        if not user_id or not article_ids:
            return func.HttpResponse(
                 "Please provide user_id and article_ids in the query string.",
                 status_code=400
            )
        
        if 'loaded_model' not in cache or 'all_categories' not in cache:
            try:
                # Charger le modèle et les données nécessaires
                cache['loaded_model'] = joblib.load('svd.joblib')
                user_article_interaction_scaled = pd.read_csv('user_article_interaction_scaled.csv')
                cache['all_categories'] = user_article_interaction_scaled['article_id'].unique()
            except Exception as e:
                logging.error(f"Error loading model or data: {e}")
                return func.HttpResponse(
                    "Failed to load model or data. Please check the server configurations.",
                    status_code=500
                )

        # Convertir les article_ids en liste d'entiers
        article_ids = list(map(int, article_ids.split(',')))

        # Préparer les données pour la prédiction
        new_user_data = pd.DataFrame({
            'user_id': [int(user_id)]*len(article_ids), 
            'article_id': article_ids, 
            'rating': [1]*len(article_ids)
        })
        
        new_user_all_categories = pd.DataFrame({'article_id': all_categories})
        new_user_all_categories['user_id'] = int(user_id)
        rating_map = new_user_data.set_index('article_id')['rating'].to_dict()
        new_user_all_categories['rating'] = new_user_all_categories['article_id'].map(rating_map).fillna(0)

        reader = Reader(rating_scale=(0, 5))
        data = Dataset.load_from_df(new_user_all_categories[['user_id', 'article_id', 'rating']], reader)
        trainset = data.build_full_trainset()

        loaded_model.fit(trainset)

        # Faire des prédictions pour l'utilisateur donné
        predictions = []
        for article_id in all_categories:
            if article_id not in article_ids:  # Exclure les articles déjà lus
                predictions.append((article_id, loaded_model.predict(int(user_id), article_id).est))

        # Trier les prédictions par score décroissant et prendre les 5 premières
        recommendations = sorted(predictions, key=lambda x: x[1], reverse=True)[:5]

        # Renvoyer les IDs des articles recommandés
        recommended_article_ids = [str(rec[0]) for rec in recommendations]
        return func.HttpResponse(",".join(recommended_article_ids))
    except Exception as e:
        logging.error(f"Error during processing: {e}")
        return func.HttpResponse(
            #  "An error occurred while processing your request. Please try again later.",
             e,
             status_code=500
        )
